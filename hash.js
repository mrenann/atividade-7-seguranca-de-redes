const CryptoJS = require("crypto-js");
// Usando Salt unico para questão de exemplificação
const salt = CryptoJS.lib.WordArray.random(128 / 8);
const secret = "Segredo secreto"

function CryptoJSMD5(textToCipher){
    // O Hash tem tamanho de 16 Bytes, por padrão
    let md5 = CryptoJS.MD5(textToCipher) + '';
    return md5
}

function CryptoJSSHA1(textToCipher){
    // O Hash tem tamanho de 20 Bytes, por padrão
    let md5 = CryptoJS.SHA1(textToCipher) + '';
    return md5
}

function CryptoJSSHA2(textToCipher){
    // Hash de tamanho de 28 Bytes
    let sha224 = CryptoJS.SHA224(textToCipher) + "";

    // Hash de tamanho de 32 Bytes
    let sha256 = CryptoJS.SHA256(textToCipher) + "";

    // Hash de tamanho de 46 Bytes
    let sha384 = CryptoJS.SHA384(textToCipher) + "";

    // Hash de tamanho de 64 Bytes
    let sha512 = CryptoJS.SHA512(textToCipher) + "";
    return {sha224, sha256, sha384, sha512};
}

function CryptoJSSHA3(textToCipher){
    // Hash de tamanho de 64 Bytes, esse é o tamanho padrão do SHA3
    let len64 = CryptoJS.SHA3(textToCipher, { outputLength: 512 }) + '';
    
    // Hash de tamanho de 48 Bytes
    let len48 = CryptoJS.SHA3(textToCipher, { outputLength: 384 }) + '';
    
    // Hash de tamanho de 32 Bytes
    let len32 = CryptoJS.SHA3(textToCipher, { outputLength: 256 }) + '';
    
    // Hash de tamanho de 28 Bytes
    let len28 = CryptoJS.SHA3(textToCipher, { outputLength: 224 }) + '';
    return {
        len64, len48,
        len32, len28,
    }
}

function CryptoJSRIPEMD160(textToCipher){
    // O Hash tem tamanho de 20 Bytes, por padrão
    let ripemd160 = CryptoJS.RIPEMD160(textToCipher) + '';
    return ripemd160
}

function CryptoJSHMAC(textToCipher){
    // O Hash tem tamanho de 16 Bytes, por padrão
    let md5 = CryptoJS.HmacMD5(textToCipher, secret) + '';

    // O Hash tem tamanho de 20 Bytes, por padrão
    let sha1 = CryptoJS.HmacSHA1(textToCipher, secret) + '';

    // O Hash tem tamanho de 28 Bytes, por padrão
    let sha224 = CryptoJS.HmacSHA224(textToCipher, secret) + '';
    
    // O Hash tem tamanho de 32 Bytes, por padrão
    let sha256 = CryptoJS.HmacSHA256(textToCipher, secret) + '';
    
    // O Hash tem tamanho de 48 Bytes, por padrão
    let sha384 = CryptoJS.HmacSHA384(textToCipher, secret) + '';
    
    // O Hash tem tamanho de 64 Bytes, por padrão
    let sha512 = CryptoJS.HmacSHA512(textToCipher, secret) + '';
    
    // O Hash tem tamanho de 64 Bytes, por padrão
    let sha3 = CryptoJS.HmacSHA3(textToCipher, secret) + '';
    
    // O Hash tem tamanho de 64 Bytes, por padrão
    let ripemd160 = CryptoJS.HmacRIPEMD160(textToCipher, secret) + '';
    return {
        md5, sha1, sha224,
        sha256, sha384, sha512,
        sha3, ripemd160
    }
}

function CryptoJSPBKDF2(textToCipher){
    // O Hash tem tamanho de 16 Bytes
    let onlySalt = CryptoJS.PBKDF2(textToCipher, salt) + '';

    // O Hash tem tamanho de 16 Bytes
    let key128Bits = CryptoJS.PBKDF2(textToCipher, salt, {
      keySize: 128 / 32
    }) + '';

    // O Hash tem tamanho de 32 Bytes
    let key256Bits = CryptoJS.PBKDF2(textToCipher, salt, {
      keySize: 256 / 32
    }) + '';

    // O Hash tem tamanho de 64 Bytes
    let key512Bits = CryptoJS.PBKDF2(textToCipher, salt, {
      keySize: 512 / 32
    }) + '';

    // O Hash tem tamanho de 64 Bytes
    let key512Bits1000Iterations = CryptoJS.PBKDF2(textToCipher, salt, {
      keySize: 512 / 32,
      iterations: 1000
    }) + '';
    return {
        onlySalt, key128Bits, key256Bits,
        key512Bits, key512Bits1000Iterations
    }
}

const md5 = CryptoJSMD5("SenhaForte");
const sha1 = CryptoJSSHA1("SenhaForte");
const sha2 = CryptoJSSHA2("SenhaForte");
const sha3 = CryptoJSSHA3("SenhaForte");
const ripemd160 = CryptoJSRIPEMD160("SenhaForte");
const hmac =CryptoJSHMAC("SenhaForte");
const pdbkdf2 =CryptoJSPBKDF2("SenhaForte");
console.log({
    MD5: md5,
    SHA1: sha1,
    SHA2:{
        SHA224: sha2.sha224,
        SHA256: sha2.sha256,
        SHA384: sha2.sha384,
        SHA512: sha2.sha512,
    },
    SHA3:{
        length64: sha3.len64,
        length48: sha3.len48,
        length32: sha3.len32,
        length28: sha3.len28,
    },
    RIPEMD160: ripemd160,
    HMAC:{ ...hmac },
    PBKDF2: { ...pdbkdf2 }
})
